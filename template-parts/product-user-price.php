<?php 
$reseller_price = get_post_meta(get_the_ID(), "wsb_reseller_price", true);
$agen_price = get_post_meta(get_the_ID(), "wsb_agen_price", true);
?>
<?php if(!empty($reseller_price) || !empty($agen_price)): ?>
<table class="woocommerce-table mt-20">
    <?php if($reseller_price): ?>
    <tr class="wpu-user-price wpu-user-price--reseller">
        <td><strong><?php echo wp_sprintf(__('%s Price', WEBFORIA_USER_PRICE_DOMAIN), 'Reseller' ) ?></strong></td>
        <td><?php echo wc_price($reseller_price) ?></td>
    </tr>
    <?php endif ?>

     <?php if($agen_price): ?>
     <tr class="wpu-user-price wpu-user-price--reseller">
        <td><strong><?php echo wp_sprintf(__('%s Price', WEBFORIA_USER_PRICE_DOMAIN), 'Agen' ) ?></strong></td>
        <td><?php echo wc_price($agen_price) ?></td>
    </tr>
    <?php endif ?>

</table>
<?php endif ?>