
<?php
$reseller_price = get_post_meta(get_the_ID(), "wsb_reseller_price", true);
$agen_price = get_post_meta(get_the_ID(), "wsb_agen_price", true);
?>
<?php if(!empty($reseller_price) || !empty($agen_price)): ?>
<div class="wbs-user-prices price">
    <?php if (!empty($reseller_price)): ?>
        <div class="wup-user-price wup-user-price--reseller mt-5">
            <?php echo wp_sprintf(__('%s Price', WEBFORIA_USER_PRICE_DOMAIN), 'Reseller' ) ?> : 
            <?php echo wc_price($reseller_price) ?>
        </div>
    <?php endif ?>

    <?php if (!empty($agen_price)): ?>
        <div class="wup-user-price wup-user-price--reseller mt-5">
            <?php echo wp_sprintf(__('%s Price', WEBFORIA_USER_PRICE_DOMAIN), 'Agen') ?> :
            <?php echo wc_price($agen_price) ?>
        </div>
    <?php endif?>
</div>
<?php endif ?>