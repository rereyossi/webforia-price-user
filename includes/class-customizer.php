<?php
namespace Webforia_User_Price;
use Retheme\Customizer_Base;

class Customizer extends Customizer_Base
{
    public function __construct()
    {
        \Kirki::add_config('webforia_user_price_customizer', array(
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ));

        $this->customizer();
    }

    public function customizer()
    {
        $customizer = new Customizer_Base;

         $this->add_section('', array(
            'user_price' => array(esc_attr__('User Price', WEBFORIA_USER_PRICE_DOMAIN)),
        ));

        $section = 'user_price_section';

        $customizer->add_field(array(
            'type' => 'toggle',
            'settings' => 'user_price_on_shop',
            'label' => __('Price on Product Loop', WEBFORIA_USER_PRICE_DOMAIN),
            'description' => __('Show special price on shop page', WEBFORIA_USER_PRICE_DOMAIN),
            'section' => $section,
            'default' => false,
        ));
        $customizer->add_field(array(
            'type' => 'toggle',
            'settings' => 'user_price_on_all_user',
            'label' => __('Show for all users', WEBFORIA_USER_PRICE_DOMAIN),
            'description' => __('Show all special price for all user', WEBFORIA_USER_PRICE_DOMAIN),
            'section' => $section,
            'default' => true,
        ));

    }

}
