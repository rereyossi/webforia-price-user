<?php
/*=================================================;
/* GET TEMPLATE
/*================================================= */
function wup_get_template_part($template, $data = '')
{
    $loader = new Webforia_User_Price\Template_Loader;

    $loader->set_template_data($data)
        ->get_template_part($template);

}

/*=================================================;
/* CHECK USER ROLE
/*================================================= */
function wup_current_user_role($userroleonly = true)
{
    global $current_user;

    $user_role = $current_user;

    if ($userroleonly) {
        $user_roles = $current_user->roles;
        $user_role = array_shift($user_roles);
        if ($user_role == null) {
            $user_role = 'visitor';
        }
    }

    return $user_role;
}
