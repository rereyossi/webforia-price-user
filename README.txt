== Webforia User Price ==
Contributors: Webforia User Price
Tags: shop
Requires at least: 5.0
Tested up to: 5.6
Stable tag: 2.0
Requires PHP: 7.0
WC requires at least: 5.0
WC tested up to: 5.0

== Description ==
Plugin WooCommerce untuk memberikan harga khusus untuk member dengan role agen dan reseller

== Changelog ==
= 1.0.1 (Released on 9 Agustus 2021) =
*Fix: label user agen dihalaman tambah user

= 1.0.0 Initial Released =
